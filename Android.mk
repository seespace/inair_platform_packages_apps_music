LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := optional
ifdef DOLBY_DAP
LOCAL_JAVA_LIBRARIES += framework_ext
else
LOCAL_STATIC_JAVA_LIBRARIES := libmds
endif #DOLBY_DAP

LOCAL_SRC_FILES := $(call all-java-files-under, src) \
	src/com/android/music/IMediaPlaybackService.aidl

LOCAL_PACKAGE_NAME := Music

LOCAL_PROGUARD_FLAG_FILES := proguard.flags
LOCAL_CERTIFICATE := platform

include $(BUILD_PACKAGE)

ifndef DOLBY_DAP
include $(CLEAR_VARS)
LOCAL_PREBUILT_STATIC_JAVA_LIBRARIES := libmds:ds.jar
include $(BUILD_MULTI_PREBUILT)
endif

# Use the folloing include to make our test apk.
include $(call all-makefiles-under,$(LOCAL_PATH))
